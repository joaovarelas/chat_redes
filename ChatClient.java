import java.io.*;
import java.net.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class ChatClient {

    // Variáveis relacionadas com a interface gráfica --- * NÃO MODIFICAR *
    JFrame frame = new JFrame("Chat Client");
    private JTextField chatBox = new JTextField();
    private JTextArea chatArea = new JTextArea();

    // --- Fim das variáveis relacionadas coma interface gráfica

    // Se for necessário adicionar variáveis ao objecto ChatClient, devem
    // ser colocadas aqui

    Socket socket;
    String server;
    int port;

    String lastCmd;
    PrintWriter outStream;
    BufferedReader inStream;


    // Método a usar para acrescentar uma string à caixa de texto
    // * NÃO MODIFICAR *
    public void printMessage(final String message) {
	chatArea.append(message);
    }


    // Construtor
    public ChatClient(String server, int port) throws IOException {
	this.server = server;
	this.port = port;
	// Inicialização da interface gráfica --- * NÃO MODIFICAR *
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	JPanel panel = new JPanel();
	panel.setLayout(new BorderLayout());
	panel.add(chatBox);
	frame.setLayout(new BorderLayout());
	frame.add(panel, BorderLayout.SOUTH);
	frame.add(new JScrollPane(chatArea), BorderLayout.CENTER);
	frame.setSize(500, 300);
	//Calculate the frame location
	Toolkit toolkit = Toolkit.getDefaultToolkit();
	Dimension screenSize = toolkit.getScreenSize();
	// Center Chat Window 
	frame.setLocation((screenSize.width - frame.getWidth()) / 2,
			  (screenSize.height - frame.getHeight()) / 2);
	frame.setVisible(true);
	chatArea.setEditable(false);
	chatBox.setEditable(true);
	chatBox.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
		    try {
			newMessage(chatBox.getText());
		    } catch (IOException ex) {} finally {
			chatBox.setText("");
		    }
		}
	    });
	// --- Fim da inicialização da interface gráfica

    }


    public String parseLine(String response) {

	String[] event = response.split(" ");
	String[] action = this.lastCmd.split(" ");

	switch (event[0]) {
	case "MESSAGE":
	    String msg = "";
	    int i = 2;
	    while (i < event.length) msg += " "+event[i++];
	    return String.format("%s:%s\n", event[1], msg);
	case "PRIVATE":
	    return String.format("*PRIVATE* %s: %s\n", event[1], event[2]);
	case "JOINED":
	    return String.format("* %s has joined the channel *\n", event[1]);
	case "LEFT":
	    return String.format("* %s has left the channel *\n", event[1]);
	case "NEWNICK":
	    return String.format("* %s is now known as %s *\n", event[1], event[2]);
	case "BYE":
	    try{
		inStream.close();
		outStream.close();
	    }catch(IOException ex){
		System.out.println("Exception at stream close(): "+ex);
	    }
	    
	    System.exit(0);
	case "OK":

	    switch (action[0]) {
	    case "/nick":
		return String.format("* You are now known as %s *\n", action[1]);
	    case "/join":
		return String.format("* You joined %s *\n", action[1]);
	    case "/leave":
		return String.format("* You left the channel *\n");
	    }

	case "ERROR":

	    switch (action[0]) {
	    case "/nick":
		if(action.length < 2)
		    return String.format("ERROR: Nickname cannot be empty\n");
		else
		    return String.format("ERROR: Nickname already in use\n");
	    case "/priv":
		return String.format("ERROR: User does not exist\n");
	    default:
		return String.format("ERROR: Unknown command\n");
	    }

	}

	return null; // Debug => Esta func tem de retornar String
    }


    // Método invocado sempre que o utilizador insere uma mensagem
    // na caixa de entrada
    public void newMessage(String message) throws IOException {
	// PREENCHER AQUI com código que envia a mensagem ao servidor
	this.lastCmd = message;
	outStream.println(message); // Send our message
    }


    // Método principal do objecto
    public void run() throws IOException {
	// PREENCHER AQUI
	socket = new Socket(server, port);
	
	outStream = new PrintWriter(socket.getOutputStream(), true);
	inStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));

	while (true) {
	    try {
		String response = inStream.readLine(); // Receive message
		printMessage(parseLine(response)); // Parse and print
		int len = chatArea.getDocument().getLength(); // Length chat text lines
		chatArea.setCaretPosition(len); // Scroll to bottom
	    } catch (IOException ex) {
		System.out.println("Exception at run(): " + ex);
	    }
	}

    }


    // Instancia o ChatClient e arranca-o invocando o seu método run()
    // * NÃO MODIFICAR *
    public static void main(String[] args) throws IOException {
	if(args.length < 2){
	    System.out.println("Port error, please run as $ java ChatClient <address> <port>");
	    return;
	}

	ChatClient client = new ChatClient(args[0], Integer.parseInt(args[1]));
	client.run();
    }

}
