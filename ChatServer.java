import java.io.*;
import java.net.*;
import java.nio.*;
import java.nio.channels.*;
import java.nio.charset.*;
import java.util.*;


class ClientInfo {
    String nick, channel;
    ByteBuffer buffer;


    // Construtor
    ClientInfo(ByteBuffer buf) {
	this.nick = null;
	this.channel = null;
	this.buffer = buf;
    }


    // Verifica se nickname ja esta em uso
    static boolean nickAvailable(String nick, Selector s) {
	for (SelectionKey k: s.keys()) {
	    if (k.isAcceptable()) continue;

	    ClientInfo info = (ClientInfo) k.attachment();
	    if (info == null) continue;

	    if (nick.equals(info.getNick()))
		return false;
	}

	return true;
    }

    // Getters & Setters
    String getNick() {
	return this.nick;
    }
    String getChannel() {
	return this.channel;
    }
    ByteBuffer getBuffer() {
	return this.buffer;
    }

    void setNick(String nick) {
	this.nick = nick;
    }
    void setChannel(String channel) {
	this.channel = channel;
    }


    // Estados
    boolean initState() {
	return (this.nick == null);
    }
    boolean outsideState() {
	return (this.channel == null);
    }
    boolean insideState() {
	return (this.channel != null);
    }
}



class Convert {
    // Conversoes
    static ByteBuffer str2bb(String s) {
	return ByteBuffer.wrap(s.getBytes());
    }
    static String bb2str(ByteBuffer b) {
	return new String(b.array(), Charset.forName("UTF8"));
    }
}





// Main Class
public class ChatServer {
    static private final Charset charset = Charset.forName("UTF8");
    static private final CharsetDecoder decoder = charset.newDecoder();
    static private final int size = (int) Math.pow(2, 16);


    // Process Buffers
    static private boolean processBuffer(SocketChannel sc, Selector selector,
					 SelectionKey activeKey) throws IOException{

	if (activeKey.attachment() == null)
	    activeKey.attach(new ClientInfo(ByteBuffer.allocate(size)));


	// Client Buffer
	ClientInfo clientInfo = (ClientInfo) activeKey.attachment();
	ByteBuffer clientBuffer = clientInfo.getBuffer();


	// Temp buffer to process received data
	ByteBuffer buffer = ByteBuffer.allocate(size);
	buffer.clear();
	sc.read(buffer);
	int pos = buffer.position();
	buffer.flip();

	// If no data, close the connection
	if (buffer.limit() == 0)
	    return false;


	// Buffering de acordo com a explicacao do Prof.
	boolean ok = true;
	for (int i = 0; i < pos; i++) {
	    if ((char) buffer.get(i) == '\n') {
		byte[] dst = new byte[clientBuffer.position()];
		clientBuffer.rewind();
		clientBuffer.get(dst);
		String cmd = new String(dst);

		
		/* Process Command */
		ok = processCmd(cmd, sc, selector, clientInfo);

		
		clientBuffer.clear();
		// Retira overflow de bytes anteriores
		clientBuffer.put(new byte[size]); 
		clientBuffer.rewind();
	    } else {
		clientBuffer.put(buffer.get(i));
	    }
	}


	return ok;
    }





    static private boolean processCmd(String line, SocketChannel sc, Selector selector,
				      ClientInfo clientInfo) throws IOException {

	Set < SelectionKey > keys = selector.keys();
	Iterator < SelectionKey > it = keys.iterator();

	if (line.length() < 1 || (line.length() == 1 && line.charAt(0) == '/')) {
	    sc.write(Convert.str2bb("ERROR\n"));
	    return true;
	}


	// Message
	if (line.charAt(0) != '/' || (line.charAt(0) == '/' && line.charAt(1) == '/')) {
	    // Escape first '/'
	    if (line.charAt(0) == '/')
		line = new String(line.substring(1));

	    if (clientInfo.insideState()) {
		String channel = clientInfo.getChannel();
		String msg = line;

		for (SelectionKey k: keys) {
		    if (k.isAcceptable()) continue;

		    ClientInfo otherUser = (ClientInfo) k.attachment();
		    if (otherUser == null) continue;

		    if (channel.equals(otherUser.getChannel()))
			((SocketChannel) k.channel())
			    .write(Convert.str2bb("MESSAGE " + clientInfo.getNick() + " " + msg + "\n"));
		}

	    } else sc.write(Convert.str2bb("ERROR\n"));

	    return true;
	}



	// Process command
	line = new String(line.substring(1));
	String cmd = line.split(" ")[0];

	if (cmd.equals("nick")) {
	    String[] lines = line.split(" ");
	    if (lines.length < 2) {
		sc.write(Convert.str2bb("ERROR\n"));
		return true;
	    }

	    String newNick = lines[1];

	    if (!ClientInfo.nickAvailable(newNick, selector)) {
		sc.write(Convert.str2bb("ERROR\n"));
		return true;
	    }

	    if (clientInfo.insideState()) {
		String oldNick = clientInfo.getNick();
		String channel = clientInfo.getChannel();
		for (SelectionKey k: keys) {
		    if (k.isAcceptable()) continue;

		    ClientInfo otherUser = (ClientInfo) k.attachment();
		    if (otherUser == null) continue;

		    if (clientInfo != otherUser && channel.equals(otherUser.getChannel()))
			((SocketChannel) k.channel())
			    .write(Convert.str2bb("NEWNICK " + oldNick + " " + newNick + "\n"));
		}
	    }

	    clientInfo.setNick(newNick); // outside State
	    sc.write(Convert.str2bb("OK\n"));
	    return true;


	} else if (cmd.equals("join")) {

	    if (!clientInfo.initState()) {
		String[] lines = line.split(" ");
		if (lines.length < 2) {
		    sc.write(Convert.str2bb("ERROR\n"));
		    return true;
		}

		String newChannel = lines[1];

		for (SelectionKey k: keys) {
		    if (k.isAcceptable()) continue;

		    ClientInfo otherUser = (ClientInfo) k.attachment();
		    if (otherUser == null) continue;


		    if (clientInfo.insideState()) {
			String oldChannel = clientInfo.getChannel();
			if (clientInfo != otherUser && oldChannel.equals(otherUser.getChannel()))
			    ((SocketChannel) k.channel())
				.write(Convert.str2bb("LEFT " + clientInfo.getNick() + "\n"));
		    }


		    if (clientInfo != otherUser && newChannel.equals(otherUser.getChannel()))
			((SocketChannel) k.channel())
			    .write(Convert.str2bb("JOINED " + clientInfo.getNick() + "\n"));
		}

		clientInfo.setChannel(newChannel);
		sc.write(Convert.str2bb("OK\n"));
		return true;
	    }

	} else if (cmd.equals("leave")) {

	    if (clientInfo.insideState()) {

		String channel = clientInfo.getChannel();
		String newChannel = null; // estado volta a "outsideState"

		for (SelectionKey k: keys) {
		    if (k.isAcceptable()) continue;
		    ClientInfo otherUser = (ClientInfo) k.attachment();
		    if (otherUser == null) continue;
		    if (clientInfo != otherUser && channel.equals(otherUser.getChannel()))
			((SocketChannel) k.channel())
			    .write(Convert.str2bb("LEFT " + clientInfo.getNick() + "\n"));
		}

		clientInfo.setChannel(newChannel);
		sc.write(Convert.str2bb("OK\n"));
		return true;
	    }

	} else if (cmd.equals("bye")) {

	    if (clientInfo.insideState()) {
		String channel = clientInfo.getChannel();
		for (SelectionKey k: keys) {
		    if (k.isAcceptable()) continue;

		    ClientInfo otherUser = (ClientInfo) k.attachment();
		    if (otherUser == null) continue;

		    if (clientInfo != otherUser && channel.equals(otherUser.getChannel()))
			((SocketChannel) k.channel())
			    .write(Convert.str2bb("LEFT " + clientInfo.getNick() + "\n"));
		}
	    }

	    sc.write(Convert.str2bb("BYE\n"));
	    return false; // fecha conexao

	} else if (cmd.equals("priv")) {

	    if (!clientInfo.initState()) {
		String[] lines = line.split(" ");
		if (lines.length < 3) {
		    sc.write(Convert.str2bb("ERROR\n"));
		    return true;
		}

		String nick = lines[1];
		String msg = lines[2];

		if (nick.equals(clientInfo.getNick()) || ClientInfo.nickAvailable(nick, selector)) {
		    sc.write(Convert.str2bb("ERROR\n"));
		    return true;
		}

		for (SelectionKey k: keys) {
		    if (k.isAcceptable()) continue;

		    ClientInfo otherUser = (ClientInfo) k.attachment();
		    if (otherUser == null) continue;

		    if (clientInfo != otherUser && nick.equals(otherUser.getNick()))
			((SocketChannel) k.channel())
			    .write(Convert.str2bb("PRIVATE " + clientInfo.getNick() + " " + msg + "\n"));
		}

		sc.write(Convert.str2bb("OK\n"));
		return true;
	    }

	}

	sc.write(Convert.str2bb("ERROR\n"));
	return true;

    }










    // Main
    static public void main(String args[]) throws Exception {
	// Parse port from command line
	if(args.length < 1){
	    System.out.println("Port error, please run as $ java ChatServer <port>");
	    return;
	}
	  
	int port = Integer.parseInt(args[0]);
	try {
	    // Instead of creating a ServerSocket, create a ServerSocketChannel
	    ServerSocketChannel ssc = ServerSocketChannel.open();

	    // Set it to non-blocking, so we can use select
	    ssc.configureBlocking(false);

	    // Get the Socket connected to this channel, and bind it to the
	    // listening port
	    ServerSocket ss = ssc.socket();
	    InetSocketAddress isa = new InetSocketAddress(port);
	    ss.bind(isa);

	    // Create a new Selector for selecting
	    Selector selector = Selector.open();

	    // Register the ServerSocketChannel, so we can listen for incoming
	    // connections
	    ssc.register(selector, SelectionKey.OP_ACCEPT);
	    System.out.println("Listening on port " + port);

	    while (true) {
		// See if we've had any activity -- either an incoming connection,
		// or incoming data on an existing connection
		int num = selector.select();

		// If we don't have any activity, loop around and wait again
		if (num == 0) continue;
		

		// Get the keys corresponding to the activity that has been
		// detected, and process them one by one
		Set < SelectionKey > keys = selector.selectedKeys();
		Iterator < SelectionKey > it = keys.iterator();
		while (it.hasNext()) {
		    // Get a key representing one of bits of I/O activity
		    SelectionKey key = it.next();

		    // What kind of activity is it?
		    if ((key.readyOps() & SelectionKey.OP_ACCEPT) ==
			SelectionKey.OP_ACCEPT) {

			// It's an incoming connection.  Register this socket with
			// the Selector so we can listen for input on it
			Socket s = ss.accept();
			System.out.println("Got connection from " + s);

			// Make sure to make it non-blocking, so we can use a selector
			// on it.
			SocketChannel sc = s.getChannel();
			sc.configureBlocking(false);

			// Register it with the selector, for reading
			sc.register(selector, SelectionKey.OP_READ);


			
		    } else if ((key.readyOps() & SelectionKey.OP_READ) ==
			       SelectionKey.OP_READ) {

			SocketChannel sc = null;

			try {

			    /*

			      It's incoming data on a connection -- process it
			    
			    */
			    sc = (SocketChannel) key.channel();
			    boolean ok = processBuffer(sc, selector, key);


			    // If the connection is dead, remove it from the selector
			    // and close it
			    if (!ok) {
				key.cancel();

				Socket s = null;
				try {
				    s = sc.socket();
				    System.out.println("Closing connection to " + s);
				    s.close();
				} catch (IOException ie) {
				    System.err.println("Error closing socket " + s + ": " + ie);
				}
			    }

			} catch (IOException ie) {

			    // On exception, remove this channel from the selector
			    key.cancel();

			    try {
				sc.close();
			    } catch (IOException ie2) {
				System.out.println(ie2);
			    }

			    System.out.println("Closed " + sc);
			}
		    }
		}

		// We remove the selected keys, because we've dealt with them.
		keys.clear();
	    }
	} catch (IOException ie) {
	    System.err.println(ie);
	}
    }


}
